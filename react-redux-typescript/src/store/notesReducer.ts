import { Action } from "./actions";
import { parseDates, setDate } from "../assistants/assistants";
import { testData } from "../assistants/testData";

export interface NotesState {
  notes: Note[];
}
export type Category = "Idea" | "Task" | "Random Thoughts";

export type Note = {
  archived: boolean;
  id: number;
  name: string;
  category: Category | "";
  content: string;
  created: string;
  dates: string | RegExpMatchArray;
};

const initialState: NotesState = {
  notes: testData,
};

export const notesReducer = (
  state: NotesState = initialState,
  action: Action
) => {
  switch (action.type) {
    case "SAVE_NOTE": {
      const existing = state.notes.find(
        (note) => note.id === action.payload.id
      );

      if (existing) {
        existing.name = action.payload.name;
        existing.category = action.payload.category;
        existing.content = action.payload.content;
        existing.dates = parseDates(action.payload.content);
        return { ...state };
      } else {
        action.payload.dates = parseDates(action.payload.content); //create new note with a new id and creation date
        action.payload.id = Math.floor(Math.random() * 1000);
        action.payload.created = setDate();
        return { ...state, notes: [...state.notes, action.payload] };
      }
    }
    case "DELETE_NOTE": {
      const deleteArr = state.notes.filter(
        (note) => note.id !== action.payload.id
      );

      return { ...state, notes: deleteArr };
    }
    case "ARCHIVE_NOTE": {
      return {
        ...state,
        notes: state.notes.map((note) =>
          note.id === action.payload.id ? { ...note, archived: true } : note
        ),
      };
    }
    case "UN_ARCHIVE_NOTE": {
      return {
        ...state,
        notes: state.notes.map((note) =>
          note.id === action.payload.id ? { ...note, archived: false } : note
        ),
      };
    }

    default:
      return state;
  }
};
