import { Note } from "./notesReducer";

type ActionType =
  | "DELETE_NOTE"
  | "ARCHIVE_NOTE"
  | "UN_ARCHIVE_NOTE"
  | "SAVE_NOTE";

export type Action = { type: ActionType; payload: Note };

export const deleteNote = (note: Note): Action => ({
  type: "DELETE_NOTE",
  payload: note,
});

export const archiveNote = (note: Note): Action => ({
  type: "ARCHIVE_NOTE",
  payload: note,
});
export const unArchiveNote = (archive: Note): Action => ({
  type: "UN_ARCHIVE_NOTE",
  payload: archive,
});

export const saveNote = (note: Note): Action => ({
  type: "SAVE_NOTE",
  payload: note,
});
