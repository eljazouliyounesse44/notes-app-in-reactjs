import { Note } from "../store/notesReducer";

export const testData = [
  {
    archived: false,
    id: 7647,
    name: "Shopping List",
    created: "12 Sep 2022",
    category: "Task",
    content: "Tomatoes, bread, cheese",
    dates: "",
  },
  {
    archived: false,
    id: 9290,
    name: "The theory of evolution",
    created: "15 Sep 2022",
    category: "Random Thoughts",
    content:
      "Evolution is the process of change in all forms of life over generations, and evolutionary biology is the study of how evolution occurs.",
    dates: "",
  },
  {
    archived: false,
    id: 1590,
    name: "New Features",
    created: "10 Sep 2022",
    category: "Idea",
    content: "Maybe I need to go to dentist 03/04/2022 and 05/04/2022 ",
    dates: "03/04/2022, 05/04/2022",
  },
  {
    archived: false,
    id: 8582,
    name: "William Gaddis ✒️",
    created: "7 Sep 2022",
    category: "Idea",
    content:
      "William Thomas Gaddis, Jr. was an American novelist. The first and longest of his five novels, The Recognitions, was named one of TIME magazine's 100 best novels from 1923 to 2005 and two others, J R and A Frolic of His Own, won the annual U.S. National Book Award for Fiction",
    dates: "",
  },
  {
    archived: false,
    id: 3755,
    name: "Books",
    created: "5 Sep 2022",
    category: "Task",
    content:
      "The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses is a book by Eric Ries describing his proposed lean startup strategy for startup companies. Ries developed the idea for the lean startup from his experiences as a startup advisor, employee, and founder.",
    dates: "",
  },
  {
    archived: false,
    id: 4600,
    name: "I so love my cat ❤️",
    created: "10 Sep 2022",
    category: "Random Thoughts",
    content:
      "Research has proven that petting a cat causes a release of the “love hormone” in humans. The technical term for this neurochemical is oxytocin, a hormone that is released when people fall in love.",
    dates: "",
  },
  {
    archived: false,
    id: 4670,
    name: "My Name Is 🎤",
    created: "11 Sep 2022",
    category: "Random Thoughts",
    content:
      "My Name Is is a song by American rapper Eminem from his second album The Slim Shady LP (1999). It is also the opening song and lead single of the album.",
    dates: "",
  },
] as Note[];
