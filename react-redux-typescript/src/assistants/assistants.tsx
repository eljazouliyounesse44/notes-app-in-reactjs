import { Note } from "../store/notesReducer";

export function setDate() {
  let month = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  let now = new Date();

  let currentDate = now.getDate();
  let currentMonth = month[now.getMonth()]; //using index of array
  let currentYear = now.getFullYear();

  return `${currentDate} ${currentMonth} ${currentYear}`;
}
export function parseDates(str: string) {
  const m1 = str.match(/\d{1}([\/.-])\d{1}\1\d{4}/g) as string[];

  return m1 ? m1 : "";
}

export function setIcon(category: string) {
  if (category === "Idea") {
    return `💡`;
  } else if (category === "Task") {
    return `🏁`;
  } else if (category === "Random Thoughts") {
    return `💭`;
  } else return `📁`;
}

export function setRandomId() {
  return Math.floor(Math.random() * 10000);
}

export const getStats = (notes: Note[]) => {
  const result = notes.reduce((acc: any, note) => {
    acc[note.category] = (acc[note.category] || 0) + 1;
    return acc;
  }, {});

  const obj = Object.assign({}, result);
  //console.log(obj);
  return obj;
};

export const getAmount = (notes: Note[]) => {
  const statsNotes = getStats(notes.filter((note) => !note.archived));

  const statsArch = getStats(notes.filter((note) => note.archived));

  const transformedA = Object.entries(statsNotes).map(([category, active]) => {
    return { category: category, active: active };
  });
  const transformedB = Object.entries(statsArch).map(([category, archived]) => {
    return { category: category, archived: archived };
  });

  const merge = (
    ...arrays: {
      category: string;
      archived: number;
    }[]
  ) => {
    const merged = {};

    arrays.forEach((item) => {
      //@ts-ignore
      return item.forEach((innerItem) =>
        //@ts-ignore
        Object.assign((merged[innerItem.category] ??= {}), innerItem)
      );
    });

    return Object.values(merged);
  };
  //@ts-ignore
  const result = merge(transformedA, transformedB);

  return result;
};
