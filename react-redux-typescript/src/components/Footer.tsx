import React from "react";

export default function Footer() {
  return (
    <div className="footer flex-parent mb-3 text-center" id="footer">
      <div className="coded-by flex-child">
        Coded by{" "}
        <a
          href="https://daria-nahorna.netlify.app/"
          target="_blank"
          className="source-code"
          title="My portfolio"
          rel="noreferrer"
        >
          Daria Nahorna
        </a>{" "}
        -{" "}
        <a
          href="https://gitlab.com/darnahorna/notes-app-in-reactjs"
          target="_blank"
          className="source-code"
          title="Note App Vanilla JS"
          rel="noreferrer"
        >
          source code
        </a>
      </div>

      <div className="email flex-child">darnahorna@gmail.com</div>
    </div>
  );
}
