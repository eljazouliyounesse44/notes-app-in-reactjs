import React from "react";

import { Note } from "../store/notesReducer";
import { getAmount, setIcon } from "../assistants/assistants";

interface StatsTableProps {
  notes: Note[];
}

type Stats = {
  category: string;
  active: number;
  archived: number;
};

export const StatsTable: React.FC<StatsTableProps> = ({ notes }) => {
  const stats = getAmount(notes) as Stats[];
  console.log(stats);

  return (
    <div className="content-table">
      <div className="content-header table d-flex justify-space-between">
        <div className="flex-row-archived">Note Category</div>
        <div className="flex-row-archived">Active</div>
        <div className="flex-row-archived">Archived</div>
      </div>
      {stats.map((note, id) => {
        return (
          <div className="content-note-stats" key={id}>
            <div className=" content-item-stats d-flex justify-space-between">
              <div className="flex-row-archived">
                {" "}
                {setIcon(note.category)} {note.category}
              </div>
              <div className="flex-row-archived">
                {note.active ? note.active : 0}
              </div>
              <div className="flex-row-archived">
                {note.archived ? note.archived : 0}{" "}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};
