import React, { Fragment } from "react";

import { Note } from "../store/notesReducer";
import { setIcon } from "../assistants/assistants";

type TableProps = {
  notes: Note[];
  deleteNote(note: Note): void;
  editNote(note: Note): void;
  archiveNote(note: Note): void;
  unArchiveNote(note: Note): void;
};

export const Table: React.FC<TableProps> = ({
  notes,
  deleteNote,
  editNote,
  archiveNote,
  unArchiveNote,
}) => {
  const onDeleteClick = (note: Note) => {
    const doDelete = window.confirm(
      "Are you sure you want to delete this note?"
    );

    if (doDelete) {
      deleteNote(note);
    }
  };
  const onArchiveClick = (note: Note) => {
    const doArchive = window.confirm(
      "Are you sure you want to archive this note?"
    );

    if (doArchive) {
      archiveNote(note);
    }
  };

  const onEditClick = (note: Note) => {
    const doArchive = window.confirm(
      "Are you sure you want to edit this note?"
    );

    if (doArchive) {
      editNote(note);
    }
  };

  const onUnArchiveClick = (note: Note) => {
    const doUnArchive = window.confirm(
      "Are you sure you want to unarchive this note?"
    );

    if (doUnArchive) {
      unArchiveNote(note);
    }
  };

  const MAX_CONTENT_LENGTH = 30;

  return (
    <div>
      <div className="content-table">
        <div className="content-header table d-flex justify-space-between">
          <div className="flex-row">Name</div>
          <div className="flex-row">Created</div>
          <div className="flex-row">Category</div>
          <div className="flex-row">Content</div>
          <div className="flex-row">Dates</div>
          <div className="flex-row">Actions</div>
        </div>
        <div className="content-note">
          {notes.map((note, id) => {
            return (
              <div
                className="content-note-item d-flex justify-space-between"
                key={id}
                data-note-id={note.id}
              >
                <div className="flex-row note-name">
                  {setIcon(note.category)} {note.name}
                </div>
                <div className="flex-row note-created">{note.created}</div>
                <div className="flex-row note-category">{note.category} </div>
                <div className="flex-row note-content">
                  {" "}
                  {note.content.substring(0, MAX_CONTENT_LENGTH)}
                  {note.content.length > MAX_CONTENT_LENGTH ? "..." : ""}
                </div>
                <div className="flex-row note-dates">{note.dates}</div>
                <div className="flex-row note-actions">
                  {note.archived ? (
                    <a
                      className="action-link"
                      title="unarchive"
                      onClick={() => onUnArchiveClick(note)}
                    >
                      📤
                    </a>
                  ) : (
                    <Fragment>
                      <a
                        className="action-link"
                        title="edit"
                        onClick={() => onEditClick(note)}
                      >
                        ✏️
                      </a>
                      {"     "}
                      <a
                        className="action-link"
                        title="archive"
                        onClick={() => onArchiveClick(note)}
                      >
                        📦
                      </a>
                      {"     "}
                      <a
                        className="action-link"
                        title="delete"
                        onClick={() => onDeleteClick(note)}
                      >
                        🗑
                      </a>
                    </Fragment>
                  )}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
