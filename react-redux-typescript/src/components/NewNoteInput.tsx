import React, { ChangeEvent, useEffect } from "react";
import { Note, Category } from "../store/notesReducer";
import { parseDates, setDate, setRandomId } from "../assistants/assistants";

interface NewNoteInputProps {
  editingNote: Note | null;
  saveNote(note: Note): void;
}

const defaultState: Note = {
  id: setRandomId(),
  name: "New Note",
  category: "Idea",
  content: "Take a note",
  created: setDate(),
  dates: "",
  archived: false,
};

export const NewNoteInput: React.FC<NewNoteInputProps> = ({
  editingNote,
  saveNote,
}) => {
  const [note, setNote] = React.useState<Note>(defaultState);

  const updateName = (event: ChangeEvent<HTMLInputElement>) => {
    setNote({
      ...note,
      name: event.target.value,
    });
  };

  const updateCategory = (event: ChangeEvent<HTMLSelectElement>) => {
    setNote({ ...note, category: event.target.value as Category });
  };

  const updateContent = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setNote({
      ...note,
      content: event.target.value,
      dates: parseDates(event.target.value),
    });
  };

  const onSaveNoteClick = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    saveNote(note);

    // setNote({
    //   ...note,
    //   id: setRandomId(),
    //   created: setDate(),
    //   archived: false,
    // });
  };

  useEffect(() => {
    if (editingNote) setNote(editingNote);
  }, [editingNote]);

  return (
    <div className="notes-preview mb-4">
      <form onSubmit={onSaveNoteClick}>
        <input
          onChange={updateName}
          value={note.name}
          className="notes-title"
          type="text"
          placeholder="New Note"
        />
        <select
          name="category"
          className="category"
          required
          value={note.category}
          onChange={updateCategory}
        >
          <option value="">Choose category</option>
          <option value="Idea">Idea</option>
          <option value="Random Thoughts">Random Thoughts</option>
          <option value="Task">Task</option>
        </select>
        <textarea
          className="notes-body"
          rows={3}
          value={note.content}
          onChange={updateContent}
        />
        <div>
          <button className="button save-note-btn" type="submit">
            Save
          </button>
        </div>
      </form>
    </div>
  );
};
