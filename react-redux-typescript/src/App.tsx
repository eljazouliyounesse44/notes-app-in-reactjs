import { useState } from "react";
import Footer from "./components/Footer";
import "./App.css";
import { NewNoteInput } from "./components/NewNoteInput";

import { useDispatch, useSelector } from "react-redux";
import {
  deleteNote,
  archiveNote,
  unArchiveNote,
  saveNote,
} from "./store/actions";
import { Note, NotesState } from "./store/notesReducer";
import { Table } from "./components/Table";
import { StatsTable } from "./components/StatsTable";

function App() {
  const [isShown, setIsShown] = useState(false);

  const [editingNote, setEditingNote] = useState<null | Note>(null);

  const unarchiveNotes = useSelector<NotesState, NotesState["notes"]>((state) =>
    state.notes.filter((note) => !note.archived)
  );

  const archivedNotes = useSelector<NotesState, NotesState["notes"]>((state) =>
    state.notes.filter((note) => note.archived)
  );

  const dispatch = useDispatch();

  const onDeleteNote = (note: Note) => {
    dispatch(deleteNote(note));
  };
  const onArchiveNote = (note: Note) => {
    dispatch(archiveNote(note));
  };
  const onEditNote = (note: Note) => {
    setIsShown(true);
    setEditingNote(note);
  };
  const onSaveNote = (note: Note) => {
    setEditingNote(null);
    setIsShown(false);
    dispatch(saveNote(note));
  };
  const onUnarchiveNote = (note: Note) => {
    dispatch(unArchiveNote(note));
  };

  const handleAddClick = () => {
    setIsShown((current) => !current);
  };

  return (
    <div className="container-app">
      <h1>Notes App ReactJS ✍️</h1>
      <h2 className="mb-4">
        It is an homework project for the Radency interneship.
      </h2>
      <Table
        notes={unarchiveNotes}
        deleteNote={onDeleteNote}
        editNote={onEditNote}
        archiveNote={onArchiveNote}
        unArchiveNote={onUnarchiveNote}
      />
      <button className="button" onClick={handleAddClick}>
        New Note
      </button>
      {isShown && (
        <NewNoteInput editingNote={editingNote} saveNote={onSaveNote} />
      )}

      <Table
        notes={archivedNotes}
        deleteNote={onDeleteNote}
        editNote={onEditNote}
        archiveNote={onArchiveNote}
        unArchiveNote={onUnarchiveNote}
      />

      <StatsTable notes={[...unarchiveNotes, ...archivedNotes]} />
      <Footer />
    </div>
  );
}

export default App;
