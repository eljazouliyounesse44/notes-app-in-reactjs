# Notes App in ReactJS ✍️

This application is part of the hometask for internship in Radenсy company.

The code for this makes use of the following HTML, CSS, TS features, React, Redux: import/export syntax, arrow functions, types, callback functions, props, useState, useEffect, store, reducer, props, state, onClick, onChange, dispatch, action.

## Task

1. Your task is to create a notes app in JS as a web app using React.js, Redux (or Redux Toolkit) and TypeScript. Users can add, edit and remove notes.

2. Notes in the table should also display a list of dates mentioned in this note as a separate column. For example, for a note “I’m gonna have a dentist appointment on the 3/5/2021, I moved it from 5/5/2021” the dates column is “3/5/2021, 5/5/2021”.

3. Users can archive notes. Archived notes are not shown in the notes list. Users can view archived notes and unarchive them.

4. There should also be a summary table which counts notes by categories: separately for active and archived. The table is updated whenever users perform some action on notes. The summary table is shown on the same page as the notes table.

5. There is no need to implement data storage. Simply create a JS variable which stores the data and prepopulate it with 7 notes so that they are shown when the page is reloaded.

6. Aim to structure components properly (container, page, component), reuse components, and reflect the apps component hierarchy:

Bonus task: Tables for “List of notes” and “Summary” should use the same table component.

## Usage

To use/test application, just click here - [Notes App Vanilla JS](https://note-app-react-js.netlify.app/).

## Getting started

Clone a repository. The files from the remote repository will be downloaded to your computer.

`git@gitlab.com:darnahorna/notes-app-in-reactjs.git`

## Author

Daria Nahorna

## Acknowledgments

This project is designed for educational purposes. The project developed the layout, design and functionality of the application.
